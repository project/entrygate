<div class="eg-main-container">
<header id="eg-header-top">
    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print $site_name ;?>" rel="<?php print $site_name; ?>" id="eg-logo" target="_blank">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>
    <?php if ($site_name): ?>
        <div id="eg-site-name">
          <strong>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
          </strong>
        </div>
   <?php endif; ?>
</header> <!---#header-top---->
  <div id="eg-main">
    <?php if ($messages): print $messages; endif; ?>
    <a id="main-content"></a>
    <?php if (!empty($tabs) && EG_USER_PAGE_ACCESS && !EG_AUTHENTICATED_USER_BYPASS): ?>
    <div id="tabs"> <?php print render($tabs); ?> </div>
    <?php endif; ?>
    <?php if ($action_links): ?>
      <ul class="action-links">
        <?php print render($action_links); ?>
      </ul>
    <?php endif; ?>
    <?php print render($page['content']); ?>
  </div>
<?php if (!empty($page['footer'])): ?>
  <footer class="eg-footer">
   <div class="row">
    <?php if ($site_name) :?>
      <div class="copyright large-12 columns">
        &copy; <?php print date('Y') . ' ' . check_plain($site_name) . ' ' . t('All rights reserved.'); ?>
      </div>
    <?php endif; ?>
   </div>
  </footer>
<?php endif; ?>
</div>
