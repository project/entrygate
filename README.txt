
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Credits


INTRODUCTION
------------

Entry Gate module prevent your site form search engine indexing and also admin can ban user by using diffrent settings provide. 

Block Search engine from indexing site and also unauthorized access while in development or in any condition you want to keep sites secure from others.

If you don't want to put your site in maintanance mode but want to prevent early access to your website during its development, then this module is very helpful for you.

INSTALLATION
------------

1. Copy the entry_gate directory to your sites/SITENAME/modules directory.

2. Enable the module at Administer >> Site building >> Modules.

3. Go for configuration setting at admin/config/development/entry_gate.

CREDITS
-------
* NewTechFusion
* Drupal Experts Team
