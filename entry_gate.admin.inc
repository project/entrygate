<?php
/**
 * @file
 * Admin settings menu callback.
 *
 * @see entry_key_menu()
 */
function entry_gate_settings_form($form, &$form_state) {
  $form['cookie_setting'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cookie Setting'),
    '#collapsible' => TRUE,
  );
  $form['cookie_setting']['entry_gate_cookie_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of Cookie'),
    '#default_value' => variable_get('entry_gate_cookie_name', 'entrygt_r4GnsBpS'),
    '#description' => t('Please enter the name of cookie which you want to set, for security purpose it should be alphanumeric string. Warning: if you will change the name then previous cookie will be expire.'),
  ); 
  $form['cookie_setting']['entry_gate_cookie_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value for Cookie'),
    '#default_value' => variable_get('entry_gate_cookie_value', '437b930db84b8079c2dd804a71936b5f'),
    '#description' => t('Please enter the value for cookie to set, for security purpose it should be complex hash string. Warning: if you will change the name then previous cookie will be expire.'),
  ); 
  $form['cookie_setting']['entry_gate_cookie_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Time Duration For Cookie to Expire'),
    '#default_value' => variable_get('entry_gate_cookie_time', '60*60*24'),
    '#description' => t('Please enter time duration to expire cookie default is <strong>"60*60*24 Seconds = 1 day"</strong> in format of <strong>"second*minut*hours*days"</strong>'),
    '#field_suffix' => '<strong>Second.</strong>',
  );
  $form['cookie_setting']['entry_gate_time_increase'] = array(
    '#type' => 'checkbox',
    '#title' => t('Increase Time Limit For Cookie On Each Page Load.'),
    '#default_value' => variable_get('entry_gate_time_increase', ''),
    '#description' => t('If checked it will increase cookies expiration time on each page request.'),
  );
  $form['user_setting'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Related Setting'),
    '#collapsible' => TRUE,
  );
  $form['user_setting']['entry_gate_bypass_authenticated_user'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow authenticated users to bypass verification process.'),
    '#default_value' => variable_get('entry_gate_bypass_authenticated_user', 'administrator'),
    '#description' => t('Warning: Use this option if you want to block all the logged in user except admin users.'),
  );
  $form['user_setting']['entry_gate_user_page_access'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow guest to access login/register page without authentication.'),
    '#default_value' => variable_get('entry_gate_user_page_access', ''),
    '#description' => t('Note: Use this option if you want to grant permission to guest users to access login/register page.'),
  );
  $form['verification_type'] = array(
    '#type' => 'fieldset',
    '#title' => t('Verification Type'),
    '#collapsible' => TRUE,
  );
  $form['verification_type']['entry_gate_verification_type'] = array(
    '#type' => 'radios',
    '#title' => t('Verification Type'),
    '#options' => array(
      'email' => t('Email Verification'),
      'key' => t('Key Verification'),
     ),
    '#default_value' => variable_get('entry_gate_verification_type', 'key'),
    '#description' => t('Select which type of verification you want to use.'),
  );
  $form['verification_type']['entry_gate_domain_names'] = array(
    '#type' => 'textarea',
    '#title' => t('Enter Domain Name List'),
    '#states' => array(
      'visible' => array(
         ':input[name="entry_gate_verification_type"]' => array('value' => 'email'),
       ),
    ),
    '#default_value' => variable_get('entry_gate_domain_names', ''),
    '#description' => t('Please enter comma separated name i.e. <strong>"example.com,mydomain.com..."</strong> etc.'),   
  );
  $form['verification_type']['entry_gate_valid_keys'] = array(
    '#type' => 'textarea',
    '#title' => t('Enter Keys'),
    '#states' => array(
      'visible' => array(
         ':input[name="entry_gate_verification_type"]' => array('value' => 'key'),
       ),
    ),
    '#default_value' => variable_get('entry_gate_valid_keys', ''),
    '#description' => t('Please enter comma saparated keys i.e. <strong>"key1,key2..."</strong> etc. Keys may be any alphanumeric value.'),
  );
  return system_settings_form($form);
}
